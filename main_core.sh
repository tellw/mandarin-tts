#!/bin/bash

case $1 in 
	1)
		filepath=$2
		if [ -f $filepath ]; then
			get_file_duration $filepath
			python utils/whisper_python_sr.py $filepath $result
		else
			echo 文件不存在
		fi
		;;
	2)
		filepath=$2
		if [ -f $filepath ];then
			cd utils/whisper_pybin
			st=`date +%s`
			whisper $filepath --language Chinese --output_format srt
			et=`date +%s`
			duration=$[ $et - $st ]
			get_file_duration $filepath
			echo "语音时长"$result"s, 处理时长"$duration"s"
		else
			echo 文件不存在
		fi
		;;
	3)
		filepath=$2
		if [ -f $filepath ];then
			cd whisper.cpp
			ffmpeg -i $filepath -ar 16000 -ac 1 -c:a pcm_s16le output.wav
			st=`date +%s`
			./main -m models/ggml-base.bin -f output.wav --language chinese
			et=`date +%s`
			duration=$[ $et - $st ]
			get_file_duration $filepath
			echo "语音时长"$result"s,处理时长"$duration"s"
		else
			echo 文件不存在
		fi
		;;
	4)
		s=$2
		if [[ $s =~ \[.+\] ]]; then
			str2array ${s:1:-1}
		else
			arr=($s)
		fi
		i=0
		n=${#arr[@]}
		echo "需要合成"$n"条语音"
		name_postfix="_mandarin_tts_"`date +%y%m%d%H%M%S`
		cd mandarin-tts/examples/aishell3
		if [ -f mandarin_tts.txt ];then
			rm mandarin_tts.txt
		fi
		st=`date +%s`
		while [[ $i<$n ]];do
			echo "生成"${arr[$i]}"对应的拼音"
			python ../../mtts/text/gp2py.py -t "${arr[$i]}">gp2py_output.txt
			ns=`sed -n 2p gp2py_output.txt`
			te=${arr[$i]}
			echo ${te:0:10}$name_postfix"|"$ns"|20 20 88 88">>mandarin_tts.txt
			echo ${te:0:10}$name_postfix".wav">>../../../synthesize_files.txt
			let i++
		done
		echo 开始合成
		python ../../mtts/synthesize.py -d cuda --config config.yaml --checkpoint ./checkpoints/checkpoint_1350000.pth.tar -i mandarin_tts.txt
		cd ../../..
		mv mandarin-tts/examples/aishell3/outputs/*.wav synthesizes
		et=`date +%s`
		duration=$[ $et - $st ]
		echo 合成时间$duration"s"
		;;
	5)
		s=$2
		if [[ $s =~ \[.+\] ]]; then
			str2array ${s:1:-1}
		else
			arr=($s)
		fi
		i=0
		n=${#arr[@]}
		echo "需要合成"$n"条语音"
		name_postfix="_mandarin_tts_"`date +%y%m%d%H%M%S`
		cd VITS-Paimon
		st=`date +%s`
		echo 开始合成
		while [[ $i < $n ]]; do
			echo 合成${arr[$i]}
			te=${arr[$i]}
			python synthesize.py $te ${te:0:10}$name_postfix
			let i++
		done
		cd ..
		mv VITS-Paimon/*.wav synthesizes
		et=`date +%s`
		duration=$[ $et-$st ]
		echo 合成时间$duration"s"
		;;
	*)
		echo 命令错误
		;;
esac