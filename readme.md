mandarin-tts目录是https://github.com/ranchlai/mandarin-tts \仓库内容

运行mandarin-tts项目前，应执行以下命令：
```
git submodule update --force --recursive --init --remote
pip install -e . f
```
加载好vocoder代码，安装mtts库（好像是全局的库，因此在一个目录里面安装好mtts，在另一个（项目）目录下也能使用mtts库，待验证）

下载模型文件（可能会有更新）
mtts检查点 https://zenodo.org/record/4912321#.YMN2-FMzakA
VocGAN https://zenodo.org/record/4743731/files/vctk_pretrained_model_3180.pt

放置到mandarin-tts/examples/aishell3/checkpoints目录下

synthesizes目录存放合成的语音文件

main_core.sh执行单条语音的合成

synthesize_article.py合成一个段落的语音

项目应在wsl下运行（好像需要在linux系统下安装mtts？还是别的原因？wsl系统是项目运行的必要条件，即wsl系统<=项目运行）

应保证mandarin-tts/mtts/models/vocoder/VocGAN/model/utils是mandarin-tts/mtts/models/vocoder/VocGAN/utils的软链接

```
cd mandarin-tts
/mtts/models/vocoder/VocGAN/model
rm -rf utils
ln -sf ../utils utils
```

如果字符串里面的单词不在词汇表里面，就报错KeyError，无法合成，这一点比较烦人