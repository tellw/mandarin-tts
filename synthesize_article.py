import subprocess
import re
import os


# 中文语句
text='协山因为想杀人，所以挖了两次坑，最后却自掘坟墓。'


# 如果存在要拼接的音频文件列表，删除
if os.path.exists('synthesize_files.txt'):
	os.remove('synthesize_files.txt')


# 根据句号分解段落中的语句，合成相应的语音
sentences=text.split('。')
for sentence in sentences:
	if len(sentence)==0:
		continue
	sentence=re.sub(r'\W','',sentence)
	subprocess.run(f'./main_core.sh 4 {sentence}',shell=True)


# 读取要拼接的音频文件列表
with open('synthesize_files.txt','r',encoding='utf8') as f:
	lines=f.read().strip().split('\n')


# 拼接音频文件
os.chdir('synthesizes')
cmd_str='ffmpeg '
tail='-filter_complex "'
tail_tail='[0:a]'
for i,line in enumerate(lines):
	cmd_str+='-i '+line+' '
	if i>0:
		tail+=f'[{i}:a]adelay=1000[a{i}];'
		tail_tail+=f'[a{i}]'
cmd_str+=tail+tail_tail+f'concat=n={len(lines)}'+':v=0:a=1" output.wav'
subprocess.run(cmd_str,shell=True)